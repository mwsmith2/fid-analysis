// Hopefully a faster way to simulate FIDs than Mathematica

#include <fstream>
#include <vector>
#include <chrono>
#include <thread>
#include <cmath>
#include <boost/numeric/odeint.hpp>
#include <algorithm>
#include <random>
#include <armadillo>

using namespace std::chrono;
using namespace boost::numeric::odeint;
using std::cout;
using std::endl;
using std::vector;
using std::ofstream;

typedef vector<double> Vec;


/*---Global Constants-------------------------------------------------------*/

const int nfids = 10000;
const int nPoints = 100000;
const int reduction = 10;
const double ti = -1.0;
const double tTotal = 10.0;
const double tf = ti + tTotal;
const double dt = 1e-5;

const double fL = 997.;
const double fRef = 950.;
const double s2n  = 100.;

const int nSteps = 5;
double Brange = 3.16227766e-3;

//strength and duration pulse; OmegaR*tpulse should equal 1/4 for pi-pulse
const double OmegaR = 50.0;
const double tPulse = 0.005;
const double gammag = 1.0; //42.6
const double gamma1 = 0.05;
const double gamma2 = 0.05;
const double pi2 = 6.283185307179586;

static double x;

/*---Inline Fucntions--------------------------------------------------------*/

template <typename T>
inline vector<T>& operator+(vector<T>& a, vector<T>& b)
{
    assert(a.size() == b.size());

    transform(a.begin(), a.end(), b.begin(), a.begin(), std::plus<T>());
    return a;
}

template <typename T>
inline vector<T>& operator-(vector<T>& a, vector<T>& b)
{
    assert(a.size() == b.size());

    transform(a.begin(), a.end(), b.begin(), a.begin(), std::minus<T>());
    return a;
}

template <typename T>
inline vector<T>& operator*(T c, vector<T>& a)
{
	for (auto it = a.begin(); it != a.end(); ++it){
		*it = c * (*it);
	}
	return a;
}

inline void cross(const Vec& u, const Vec& v, Vec& res){
	res[0] = u[1] * v[2] - u[2] * v[1];
	res[1] = u[2] * v[0] - u[0] * v[2];
	res[2] = u[0] * v[1] - u[1] * v[0];
} 

inline Vec Bfield(const double& x, const double& t){
	static Vec a = {0., 0., 0.};
	static Vec b = {0., 0., 0.};
	static double w = pi2 * fRef;

	if (t >= tPulse){
		return a;
	}else if (t <= ti + dt){
		a[2] = pi2 * (1. + x * x) * fL;
		b[2] = pi2 * (1. + x * x) * fL;
	}

	if (t < 0.0) return a;

	b[0] = OmegaR * cos(w * t);
	b[1] = OmegaR * sin(w * t);
	return b;
}

void bloch(Vec const &s, Vec &dsdt, double t){
	static Vec b = {{0., 0., 0.}};
	static Vec s1 = {{0., 0., 0.}};
	static Vec s2 = {{0., 0., 0.}};
	static double a1 = pi2 * gamma1;
	static double a2 = pi2 * gamma2;


	if (t <= tPulse + dt) b = Bfield(::x, t);
	
//	cout << "field: (" << b[0] << ", " << b[1] << ", " << b[2] << ")" << endl;

	s2[0] = a2 * s[0];
	s2[1] = a2 * s[1];
	s2[2] = a1 * s[2] - a1;

	cross(b, s, s1);
   	dsdt = s1 - s2;
}

vector<double> lowPassFilter(vector<double>& s);

void addNoise(vector<double>& s);

void printer(Vec const &s, double t);


/*---------------------------------------------------------------------------*/
/*---Main Body---------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

int main() {
	// Record the start time
	time_point <steady_clock> start, end;
	start = steady_clock::now();
	::x = 0.0;

	// Loop over each FID
   	for (int i = 0; i < nfids; i++){

   		if (i % 2000 == 0){
   			Brange *= 3.16227766;
   		}

   		if (nSteps > 1){
	   		::x = -0.5 * Brange;
	   		for (int j = 0; j < nSteps; j++){
			   	Vec s = {0., 0., 1.}; // Initial magnetic polarization
				integrate_const(runge_kutta4<Vec>(), bloch, s, ti, tf, dt, printer);
	 			::x += Brange / (nSteps - 1);
			}
		} else {
		   	Vec s = {0., 0., 1.}; // Initial magnetic polarization
			integrate_const(runge_kutta4<Vec>(), bloch, s, ti, tf, dt, printer);
		}
   	}

   	// Get the final time
   	end = steady_clock::now();

   	duration<double> elapsed_secs = end - start;
   	cout << "Simulated " << nfids << " FIDs in " << elapsed_secs.count();
   	cout << " seconds." << endl;

   	return 0;
}


/*---Function Implementations------------------------------------------------*/

vector<double> lowPassFilter(vector<double>& s){
	static vector<double> filter;
	static double wCut = pi2 * fRef;
	if (filter.size() == 0){
		filter.resize(nPoints);
		int i = 0;
		int j = nPoints-1;
		while (i < nPoints/2){
			filter[i] = pow(1.0 / (1.0 + pow((pi2 * i) / (dt * nPoints * wCut), 6)), 0.5);
			filter[j--] = filter[i++];
		}
	}

	arma::vec v1(&s[0], s.size(), false);
	arma::vec v2(&filter[0], filter.size(), false);
	arma::cx_vec fft = arma::fft(v1);

	fft = fft % v2;
	return arma::conv_to<vector<double>>::from(arma::real(arma::ifft(fft)));
}

void addNoise(vector<double>& s){
	static double sigma = -1.0;
	static std::default_random_engine gen;

	// set sigma if not set already
	if (sigma <= 0.0){
		double max = *std::max_element(s.begin(), s.end());
		sigma = max / s2n;
	}

	static std::normal_distribution<double> gaus(0.0, sigma);

	for (auto it = s.begin(); it != s.end(); it++){
		*it += gaus(gen);
	}
	// noisied up
}

void printer(Vec const &s , double t){
	static int count = 0;
	static int index = 0;
	static int fidNum = 0;
	static int stepNum = 0;
	static int refCount = (tTotal / nPoints) / dt;
	static vector<double> spin (nPoints, 0.0);
	static vector<double> time (nPoints, 0.0);
	static vector<double> spinSum (nPoints, 0.0);
	static vector<double> cosCache;
	static ofstream out;

	if (cosCache.size() == 0){
		cosCache.reserve(nPoints);
		double temp = t;
		for (int i = 0; i < nPoints; i++){
			cosCache.push_back(cos(pi2 * fRef * temp));
			temp += dt * refCount;
		}
	}

	if (count++ % refCount == 0){

	   	spin[index] = s[1] * cosCache[index];
	   	time[index++] = t;

		if (t == tf){

			if (stepNum == 0) spinSum.assign(nPoints, 0.0);

			if (stepNum == nSteps-1){

				char filename[20];
				sprintf(filename, "sim_%04i.fid", fidNum++);
				out.open(filename, ofstream::out);	

				spinSum = spinSum + spin;
				spinSum = lowPassFilter(spinSum);
				addNoise(spinSum);
				for (int i = 0; i < nPoints; i += reduction){
					out << time[i] << " " << spinSum[i] << endl;
				}
				out.close();

				index = 0; // They'll get incremented after.
				stepNum = 0;
				count = 0;

				if (fidNum % 100 == 0){
					cout << fidNum << " FIDs have been simulated." << endl;
				}

			} else {

				spinSum = spinSum + spin;
				stepNum++;
				index = 0;
				count = 0;

			}
		}
	}
}
