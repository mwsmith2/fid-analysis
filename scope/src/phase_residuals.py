#!/bin/python

import numpy as np
import sys
from ROOT import *

execfile('src/fid.py')

fid = FID(sys.argv[1])

w = 10
I2 = fid.fid_i + w
F2 = fid.fid_f - w
N2 = F2 - I2

h_fid_pol1_phs = TH1F("h_fid_pol1_phs", "FID: Pol1 Phase", int(N2), I2, F2+1)
h_fid_pol1_fit = TH1F("h_fid_pol1_fit", "FID: Pol1 Phase Fit", int(N2), I2, F2+1)
h_fid_pol1_res = TH1F("h_fid_pol1_res", "FID: Pol1 Fit Residuals", int(N2), I2, F2+1)
h_fid_pol2_phs = TH1F("h_fid_pol2_phs", "FID: Pol2 Phase", int(N2), I2, F2+1)
h_fid_pol2_fit = TH1F("h_fid_pol2_fit", "FID: Pol2 Phase Fit", int(N2), I2, F2+1)
h_fid_pol2_res = TH1F("h_fid_pol2_res", "FID: Pol2 Fit Residuals", int(N2), I2, F2+1)
h_fid_pol3_phs = TH1F("h_fid_pol3_phs", "FID: Pol3 Phase", int(N2), I2, F2+1)
h_fid_pol3_fit = TH1F("h_fid_pol3_fit", "FID: Pol3 Phase Fit", int(N2), I2, F2+1)
h_fid_pol3_res = TH1F("h_fid_pol3_res", "FID: Pol3 Fit Residuals", int(N2), I2, F2+1)

phase1, fit1 = fid.getPhaseFit(poln=1)
phase2, fit2 = fid.getPhaseFit(poln=2)
phase3, fit3 = fid.getPhaseFit(poln=3)
for i in range(N2):
	h_fid_pol1_phs.SetBinContent(i, phase1[i])
	h_fid_pol1_fit.SetBinContent(i, fit1[i])
	h_fid_pol1_res.SetBinContent(i, phase1[i] - fit1[i])
	h_fid_pol2_phs.SetBinContent(i, phase2[i])
	h_fid_pol2_fit.SetBinContent(i, fit2[i])
	h_fid_pol2_res.SetBinContent(i, phase2[i] - fit2[i])
	h_fid_pol3_phs.SetBinContent(i, phase3[i])
	h_fid_pol3_fit.SetBinContent(i, fit3[i])
	h_fid_pol3_res.SetBinContent(i, phase3[i] - fit3[i])

data_dir = 'fig/phase_residuals/'

h_fid_pol1_phs.Draw(); c1.Print(data_dir + h_fid_pol1_phs.GetName() + '.pdf')
h_fid_pol1_fit.Draw(); c1.Print(data_dir + h_fid_pol1_fit.GetName() + '.pdf')
h_fid_pol1_res.Draw(); c1.Print(data_dir + h_fid_pol1_res.GetName() + '.pdf')
h_fid_pol2_phs.Draw(); c1.Print(data_dir + h_fid_pol2_phs.GetName() + '.pdf')
h_fid_pol2_fit.Draw(); c1.Print(data_dir + h_fid_pol2_fit.GetName() + '.pdf')
h_fid_pol2_res.Draw(); c1.Print(data_dir + h_fid_pol2_res.GetName() + '.pdf')
h_fid_pol3_phs.Draw(); c1.Print(data_dir + h_fid_pol3_phs.GetName() + '.pdf')
h_fid_pol3_fit.Draw(); c1.Print(data_dir + h_fid_pol3_fit.GetName() + '.pdf')
h_fid_pol3_res.Draw(); c1.Print(data_dir + h_fid_pol3_res.GetName() + '.pdf')
