#!/bin/python

import numpy as np
from ROOT import *

execfile('src/fid.py')

data_dir = 'data/phasetest/'

fid2 = FID(data_dir + 'ch2.fid')
fid3 = FID(data_dir + 'ch3.fid')

w = 10
I2 = fid2.fid_i + w
F2 = fid2.fid_f - w
N2 = F2 - I2

I3 = fid3.fid_i + w
F3 = fid3.fid_f - w
N3 = F3 - I3

h_ch2_pol1_phs = TH1F("h_ch2_pol1_phs", "Channel 2: Pol1 Phase", N2, I2, F2+1)
h_ch2_pol1_fit = TH1F("h_ch2_pol1_fit", "Channel 2: Pol1 Phase Fit", N2, I2, F2+1)
h_ch2_pol1_res = TH1F("h_ch2_pol1_res", "Channel 2: Pol1 Fit Residuals", N2, I2, F2+1)
h_ch2_pol2_phs = TH1F("h_ch2_pol2_phs", "Channel 2: Pol2 Phase", N2, I2, F2+1)
h_ch2_pol2_fit = TH1F("h_ch2_pol2_fit", "Channel 2: Pol2 Phase Fit", N2, I2, F2+1)
h_ch2_pol2_res = TH1F("h_ch2_pol2_res", "Channel 2: Pol2 Fit Residuals", N2, I2, F2+1)
h_ch2_pol3_phs = TH1F("h_ch2_pol3_phs", "Channel 2: Pol3 Phase", N2, I2, F2+1)
h_ch2_pol3_fit = TH1F("h_ch2_pol3_fit", "Channel 2: Pol3 Phase Fit", N2, I2, F2+1)
h_ch2_pol3_res = TH1F("h_ch2_pol3_res", "Channel 2: Pol3 Fit Residuals", N2, I2, F2+1)

h_ch3_pol1_phs = TH1F("h_ch3_pol1_phs", "Channel 3: Pol1 Phase", N3, I3, F3+1)
h_ch3_pol1_fit = TH1F("h_ch3_pol1_fit", "Channel 3: Pol1 Phase Fit", N3, I3, F3+1)
h_ch3_pol1_res = TH1F("h_ch3_pol1_res", "Channel 3: Pol1 Fit Residuals", N3, I3, F3+1)
h_ch3_pol2_phs = TH1F("h_ch3_pol2_phs", "Channel 3: Pol2 Phase", N3, I3, F3+1)
h_ch3_pol2_fit = TH1F("h_ch3_pol2_fit", "Channel 3: Pol2 Phase Fit", N3, I3, F3+1)
h_ch3_pol2_res = TH1F("h_ch3_pol2_res", "Channel 3: Pol2 Fit Residuals", N3, I3, F3+1)
h_ch3_pol3_phs = TH1F("h_ch3_pol3_phs", "Channel 3: Pol3 Phase", N3, I3, F3+1)
h_ch3_pol3_fit = TH1F("h_ch3_pol3_fit", "Channel 3: Pol3 Phase Fit", N3, I3, F3+1)
h_ch3_pol3_res = TH1F("h_ch3_pol3_res", "Channel 3: Pol3 Fit Residuals", N3, I3, F3+1)

phase1, fit1 = fid2.getPhaseFit(poln=1)
phase2, fit2 = fid2.getPhaseFit(poln=2)
phase3, fit3 = fid2.getPhaseFit(poln=3)
for i in range(N2):
	h_ch2_pol1_phs.SetBinContent(i, phase1[i])
	h_ch2_pol1_fit.SetBinContent(i, fit1[i])
	h_ch2_pol1_res.SetBinContent(i, phase1[i] - fit1[i])
	h_ch2_pol2_phs.SetBinContent(i, phase2[i])
	h_ch2_pol2_fit.SetBinContent(i, fit2[i])
	h_ch2_pol2_res.SetBinContent(i, phase2[i] - fit2[i])
	h_ch2_pol3_phs.SetBinContent(i, phase3[i])
	h_ch2_pol3_fit.SetBinContent(i, fit3[i])
	h_ch2_pol3_res.SetBinContent(i, phase3[i] - fit3[i])

phase1, fit1 = fid3.getPhaseFit(poln=1)
phase2, fit2 = fid3.getPhaseFit(poln=2)
phase3, fit3 = fid3.getPhaseFit(poln=3)
for i in range(N3):
	h_ch3_pol1_phs.SetBinContent(i, phase1[i])
	h_ch3_pol1_fit.SetBinContent(i, fit1[i])
	h_ch3_pol1_res.SetBinContent(i, phase1[i] - fit1[i])
	h_ch3_pol2_phs.SetBinContent(i, phase2[i])
	h_ch3_pol2_fit.SetBinContent(i, fit2[i])
	h_ch3_pol2_res.SetBinContent(i, phase2[i] - fit2[i])
	h_ch3_pol3_phs.SetBinContent(i, phase3[i])
	h_ch3_pol3_fit.SetBinContent(i, fit3[i])
	h_ch3_pol3_res.SetBinContent(i, phase3[i] - fit3[i])


h_ch2_pol1_phs.Draw(); c1.Print(data_dir + h_ch2_pol1_phs.GetName() + '.pdf')
h_ch2_pol1_fit.Draw(); c1.Print(data_dir + h_ch2_pol1_fit.GetName() + '.pdf')
h_ch2_pol1_res.Draw(); c1.Print(data_dir + h_ch2_pol1_res.GetName() + '.pdf')
h_ch2_pol2_phs.Draw(); c1.Print(data_dir + h_ch2_pol2_phs.GetName() + '.pdf')
h_ch2_pol2_fit.Draw(); c1.Print(data_dir + h_ch2_pol2_fit.GetName() + '.pdf')
h_ch2_pol2_res.Draw(); c1.Print(data_dir + h_ch2_pol2_res.GetName() + '.pdf')
h_ch2_pol3_phs.Draw(); c1.Print(data_dir + h_ch2_pol3_phs.GetName() + '.pdf')
h_ch2_pol3_fit.Draw(); c1.Print(data_dir + h_ch2_pol3_fit.GetName() + '.pdf')
h_ch2_pol3_res.Draw(); c1.Print(data_dir + h_ch2_pol3_res.GetName() + '.pdf')

h_ch3_pol1_phs.Draw(); c1.Print(data_dir + h_ch3_pol1_phs.GetName() + '.pdf')
h_ch3_pol1_fit.Draw(); c1.Print(data_dir + h_ch3_pol1_fit.GetName() + '.pdf')
h_ch3_pol1_res.Draw(); c1.Print(data_dir + h_ch3_pol1_res.GetName() + '.pdf')
h_ch3_pol2_phs.Draw(); c1.Print(data_dir + h_ch3_pol2_phs.GetName() + '.pdf')
h_ch3_pol2_fit.Draw(); c1.Print(data_dir + h_ch3_pol2_fit.GetName() + '.pdf')
h_ch3_pol2_res.Draw(); c1.Print(data_dir + h_ch3_pol2_res.GetName() + '.pdf')
h_ch3_pol3_phs.Draw(); c1.Print(data_dir + h_ch3_pol3_phs.GetName() + '.pdf')
h_ch3_pol3_fit.Draw(); c1.Print(data_dir + h_ch3_pol3_fit.GetName() + '.pdf')
h_ch3_pol3_res.Draw(); c1.Print(data_dir + h_ch3_pol3_res.GetName() + '.pdf')


