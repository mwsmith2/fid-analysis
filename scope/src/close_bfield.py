from numpy import arctan, log, linspace
import matplotlib.pyplot as plt

x = linspace(0.5, 10, 1001)

plt.plot(x, (2 * arctan(1.0 / x) / ((1 + x**2)**0.5 + 0.5 * x**2 * log((1.0 + (1.0 + x**2)**0.5) / (-1.0 + (1.0 + x**2)**0.5))) - 1.0 / x**3))
plt.title("Geometric Effect of Finite Sized NMR Probes")
plt.xlabel("y_0 / d")
plt.ylabel("Explicit_Model - Simple_Model")
plt.savefig("/Users/TC/Desktop/close_bfield.png")