#!/bin/python

import numpy as np
import time
from ROOT import *

nsims = 10000
npoints = 14

xstart = 30.0
moment = -3.0e4
dprobe = 13.0
b0 = 1.0
grad = 0.002
dx = 2.0

rand = TRandom3()
xnoise = 0.05
fnoise = 0.002

def dipole_field(m, x):

	x += rand.Gaus(0.0, xnoise)	
	b  = b0 + grad * x
	b += m / (x + xstart) ** 3.0 
	b -= m / (x + xstart + dprobe) ** 3.0

	return b + rand.Gaus(0.0, fnoise)


print "####################################################"
print "Running dipole simulation with following parameters:"
print "m = " + str(moment)
print "d = " + str(xstart)
print "b0 = " + str(b0)
print "b1 = " + str(grad)
print "####################################################"

f1 = TF1("f1", "[0] + [1] * (1.0 / ([2] + x)^3 - 1.0 / ([2] + " + str(dprobe) + " + x)^3) + [3] * x")
#f1.FixParameter(3, 0.0)
h_p0 = TH1F("h_p0", "", 100, 0, 0)
h_p1 = TH1F("h_p1", "", 100, 0, 0)
h_p2 = TH1F("h_p2", "", 100, 0, 0)
h_p3 = TH1F("h_p3", "", 100, 0, 0)

for i in range(nsims):

	if (i % 1000 == 0):
		print str(i) + " simulations complete."

	gr = TGraphErrors(npoints)

	for n in range(npoints):

		gr.SetPoint(n, dx * n, dipole_field(moment, dx * n))
		gr.SetPointError(n, 0.0, fnoise)

	gr.Fit(f1, "Q", "", 4, 20)
	gr.Fit(f1, "Q")

	h_p0.Fill(f1.GetParameter(0))
	h_p1.Fill(f1.GetParameter(1))
	h_p2.Fill(f1.GetParameter(2))
	h_p3.Fill(f1.GetParameter(3))

c1 = TCanvas()
h_p0.Draw(); c1.Print("h_lin_p0.png")
h_p1.Draw(); c1.Print("h_lin_p1.png")
h_p2.Draw(); c1.Print("h_lin_p2.png")
h_p3.Draw(); c1.Print("h_lin_p3.png")
