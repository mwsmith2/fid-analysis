#!/bin/python

import sys

P1 = float(sys.argv[1])
V  = float(sys.argv[2]) * 1e-9

g1 = 42.6e3 # gyromagnetic ratio of hydrogen
B0 = 1.45 # Tesla

m = -P1 / g1 * 1e7 * 1e-9 # 1e7 is 4 pi / mu_0
M = m / V
x = (1 + 2 * 2.89e-7 * M) / (1 - 2.89e-7 * M) - 1
B = 4e-7 * 3.141952 * M / 3

print "The dipole strength is:  " + str(m)
print "The magnetization is:    " + str(M)
print "The susceptibility is:   " + str(x)
print "The internal B-field is: " + str(B)