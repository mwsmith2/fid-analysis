import sys
if len(sys.argv)<2:
    sys.exit("Wrong number of arguments: ConvertCSV.py fileIn1 fileIne2 ...")
for fileIn in sys.argv[1:len(sys.argv)]:
    fileOut=fileIn.replace(fileIn.split(".",1)[1],"fid")
    fileOut=fileOut.replace(")","")
    fileOut=fileOut.replace("(","")    
    print(fileIn + " written into " + fileOut)
    inf=open(fileIn,"r")
    wf=[]
    for line in inf:
        if line=='\r\n' or line=='\n': break
        words=line.split(",")
        t=float(words[0])*1000.
        x=int(float(words[1])*1000)
        wf.append("{0:> 7.03f} {1:> 4d}\n".format(t,x))
    inf.close()
    out=open(fileOut,"w")
    out.writelines(wf)
    out.close()
