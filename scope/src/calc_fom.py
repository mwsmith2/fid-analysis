#!/bin/python

def dsum_grid(r0, nrows=6, ncols=9, d=25):

	dsum = 0

	for i in range(nrows):
		for j in range(ncols):
			dsum += ((r0 + d * (i + 0.5))**2 + (d * (j + 0.5))**2)**-1.5

	return dsum * 1e9 # Convert from mm to m

r0 = 100 # storage radius - detector start # could add angle later
dB = 0.07e-6 * 1.45

fom = dB * 1e7 / dsum_grid(r0)

print "The figure of merit is " + str(fom) + "[A-m^2]"
