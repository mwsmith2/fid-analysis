#!/bin/python

import sys
import numpy as np
#import matplotlib.pyplot as plt 
import ntpath
from ROOT import *

if len(sys.argv) < 2:
	fil = 'data/sipm_only/data.txt'
else:
	fil = sys.argv[1]

if len(sys.argv) < 3:
	title = 'FID Frequencies'
else:
	title = sys.argv[2]

if len(sys.argv) < 4:
	al_arm_correction = True
else:
	al_arm_correction = bool(sys.argv[3])

# Load the data
d = np.genfromtxt(fil)
xd = d[:, 0]
df = d[:, 1]
fe = d[:, 2]
del d

f1 = TF1("f1", "[0] + [1] * (1 / ([2] + 18 + x)^3 - 1 / ([2] + 30.7 + x)^3) + [3] * x")
f1.SetLineColor(kRed)

par0 = []
par1 = []
par2 = []
par3 = []

# Now for the bootstrap
for i in range(xd.shape[0]):

	idx = range(xd.shape[0])[:i] + range(xd.shape[0])[i+1:]

	gr = TGraphErrors(xd.shape[0])

	for n in idx:

		gr.SetPoint(n, xd[n], df[n])
		gr.SetPointError(n, 0.0, fe[n])

	gr.Fit(f1, "Q", "", xd[0] + 4, xd[-1] - 4)
	gr.Fit(f1, "Q")

	par0.append(f1.GetParameter(0))
	par1.append(f1.GetParameter(1))
	par2.append(f1.GetParameter(2))
	par3.append(f1.GetParameter(3))

print np.mean(par0), par0
print "\n"
print np.mean(par1), par1
print "\n"
print np.mean(par2), par2
print "\n"
print np.mean(par3), par3

