#!/bin/python

import sys
import numpy as np
import time
from ROOT import *
from subprocess import check_output

execfile('src/fid.py')

# Make sure we have a path
if (len(sys.argv) < 2):
	print "The script needs a sim directory."
	print "usage: python sim_analyzer.py <sim_directory>"
	sys.exit(0)

#sim_dir = 'data/sim_fids/freq_47khz/'

sim_dir = sys.argv[1]
filelist = check_output(['ls', sim_dir]).split('\n')
# There is an extra line on the end for my machine
if (len(filelist[-1]) == 0): filelist.pop() 

nbin = 500
fmin = 0
fmax = 0

h_zc_freq = TH1F("h_zc_freq", "Frequency: Zero Counting Method; Frequency [kHz]; Counts", nbin, fmin, fmax)
h_cn_freq = TH1F("h_cn_freq", "Frequency: Centroid Method; Frequency [kHz]; Counts", nbin, fmin, fmax)
h_lz_freq = TH1F("h_lz_freq", "Frequency: Lorentzian Fit; Frequency [kHz]; Counts", nbin, fmin, fmax)
h_sl_freq = TH1F("h_sl_freq", "Frequency: 'Soft' Lorentzian Fit; Frequency [kHz]; Counts", nbin, fmin, fmax)
h_ex_freq = TH1F("h_ex_freq", "Frequency: Exponential Fit; Frequency [kHz]; Counts", nbin, fmin, fmax)
h_p1_freq = TH1F("h_p1_freq", "Frequency: Phase Fit to 1st Order Polynomial; Frequency [kHz]; Counts", nbin, fmin, fmax)
h_p2_freq = TH1F("h_p2_freq", "Frequency: Phase Fit to 2st Order Polynomial; Frequency [kHz]; Counts", nbin, fmin, fmax)
h_p3_freq = TH1F("h_p3_freq", "Frequency: Phase Fit to 3st Order Polynomial; Frequency [kHz]; Counts", nbin, fmin, fmax)
h_os_freq = TH1F("h_os_freq", "Frequency: Sinusoid Fit to Envelope Normalized Waveform; Frequency [kHz]; Counts", nbin, fmin, fmax)


i = 0
for f in filelist:

	if (i % 100 == 0): print str(i) + " complete."
	i += 1

	fid = FID(sim_dir + f)
	thresh = 48.0

	try: 
		freq = fid.getZeroCountFreq()
		if (freq > thresh):
			print f, freq, fid.guess
		h_zc_freq.Fill(freq)
	except:
		RuntimeError

	try:
		freq = fid.getCentroidFreq()
		if (freq > thresh):
			print f, freq, fid.guess
		h_cn_freq.Fill(freq)
	except:
		RuntimeError

	try:
		freq = fid.getLorentzianFreq()
		if (freq > thresh):
			print f, freq, fid.guess
		h_lz_freq.Fill(freq)
	except:
		RuntimeError

	try:
		freq = fid.getSoftLorentzianFreq()
		if (freq > thresh):
			print f, freq, fid.guess
		h_sl_freq.Fill(freq)
	except:
		RuntimeError

	try:
		freq = fid.getExponentialFreq()
		if (freq > thresh):
			print f, freq, fid.guess
		h_ex_freq.Fill(freq)
	except:
		RuntimeError

	try:
		freq = fid.getPhaseFreq(poln=1)
		if (freq > thresh):
			print f, freq, fid.guess
		h_p1_freq.Fill(freq)
	except:
		RuntimeError

	try:
		freq = fid.getPhaseFreq(poln=2)
		if (freq > thresh):
			print f, freq, fid.guess
		h_p2_freq.Fill(freq)
	except:
		RuntimeError

	try:
		freq = fid.getPhaseFreq(poln=3)
		if (freq > thresh):
			print f, freq, fid.guess
		h_p3_freq.Fill(freq)
	except:
		RuntimeError

	try:
		freq = fid.getOscillatorFreq()
		if (freq > thresh):
			print f, freq, fid.guess
		h_os_freq.Fill(freq)
	except:
		RuntimeError

c1 = TCanvas()

sim_dir = sim_dir.replace('data', 'fig')

h_os_freq.Draw(); c1.Print(sim_dir + h_os_freq.GetName() + '.C')
h_os_freq.Draw(); c1.Print(sim_dir + h_os_freq.GetName() + '.png')
h_p3_freq.Draw(); c1.Print(sim_dir + h_p3_freq.GetName() + '.C')
h_p3_freq.Draw(); c1.Print(sim_dir + h_p3_freq.GetName() + '.png')
h_p2_freq.Draw(); c1.Print(sim_dir + h_p2_freq.GetName() + '.C')
h_p2_freq.Draw(); c1.Print(sim_dir + h_p2_freq.GetName() + '.png')
h_p1_freq.Draw(); c1.Print(sim_dir + h_p1_freq.GetName() + '.C')
h_p1_freq.Draw(); c1.Print(sim_dir + h_p1_freq.GetName() + '.png')
h_ex_freq.Draw(); c1.Print(sim_dir + h_ex_freq.GetName() + '.C')
h_ex_freq.Draw(); c1.Print(sim_dir + h_ex_freq.GetName() + '.png')
h_sl_freq.Draw(); c1.Print(sim_dir + h_sl_freq.GetName() + '.C')
h_sl_freq.Draw(); c1.Print(sim_dir + h_sl_freq.GetName() + '.png')
h_lz_freq.Draw(); c1.Print(sim_dir + h_lz_freq.GetName() + '.C')
h_lz_freq.Draw(); c1.Print(sim_dir + h_lz_freq.GetName() + '.png')
h_cn_freq.Draw(); c1.Print(sim_dir + h_cn_freq.GetName() + '.C')
h_cn_freq.Draw(); c1.Print(sim_dir + h_cn_freq.GetName() + '.png')
h_cn_freq.Draw(); c1.Print(sim_dir + h_cn_freq.GetName() + '.C')
h_cn_freq.Draw(); c1.Print(sim_dir + h_cn_freq.GetName() + '.png')
h_zc_freq.Draw(); c1.Print(sim_dir + h_zc_freq.GetName() + '.C')
h_zc_freq.Draw(); c1.Print(sim_dir + h_zc_freq.GetName() + '.png')
