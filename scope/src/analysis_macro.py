#!/bin/python

import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import sys
import ntpath
from ROOT import *

# Rewriting the analysis code in python

stub = 'fig/' + ntpath.basename(sys.argv[1]).replace('.fid','_')

# Load the file first
temp = np.genfromtxt(sys.argv[1])

time = temp[:, 0]
wf = temp[:, 1]
N  = wf.shape[0]
dt = (time[-1] - time[0]) / (N - 1)

del temp

# Center the amplitude (easier to count zeros)
wf -= wf.mean()

# Plot the FID
plt.plot(time, wf)
plt.title('FID')
plt.xlabel('Time [ms]')
plt.ylabel('Amplitude [arb. units]')
plt.savefig(stub + 'fid.png')
plt.close()

# Estimate the noise
noise = wf[:100].std()
if (noise > wf[-100:].std()):
	noise = wf[-100:].std()

# Compute the fft
fft  = np.fft.rfft(wf)# / N**0.5
freq = np.fft.fftfreq(wf.shape[0], d=dt)[:N/2+1]
freq[-1] *= -1 # Stupid trick since rfftfreq helper function doesn't exist
power = np.abs(fft)**2

# FFT plot
plt.plot(freq, power)
plt.title('Fourier Transform')
plt.xlabel('Frequency')
plt.ylabel('Power')
plt.savefig(stub + 'fft.png')
plt.close()

# Compute the envelope function
wf_im = np.fft.irfft(fft * -1j)
env = np.sqrt(wf * wf + wf_im * wf_im)

i = 0
f = 10000
# Envelope plot
plt.plot(time[i:f], env[i:f])
plt.plot(time[i:f], wf[i:f])
plt.plot(time[i:f], wf_im[i:f])
plt.title('Envelope Function')
plt.xlabel('Time [ms]')
plt.ylabel('Amplitude')
plt.savefig(stub + 'env.png')
plt.close()

# Count the zeros
ncross = 0
pos = True

fid_i = -1
fid_f = -1

start = 0
w = 20
while (abs(wf[start:start+w]).mean() < 5 * noise): start += 1

for i in range(start, N - w):

	if (wf[i:i+w].std() < noise * 5): break

	if (abs(wf[i]) < noise * 3): continue

	# Set the start point of the FID
	if (fid_i == -1):
		fid_i = i
		pos = (wf[i] >= 0)

	if (pos != (wf[i] >= 0)):
		pos = (wf[i] >= 0)
		ncross += 1
		fid_f = i

print "FID begins at time[" + str(fid_i) + "] = " + str(time[fid_i]) + "\n"
print "FID ends at time[" + str(fid_f) + "] = " + str(time[fid_f]) + "\n"

freq_zeros = 0.5 * ncross / (time[fid_f] - time[fid_i])

print "\nNumber of Zeros, Frequency Estimate"
print ncross, freq_zeros

# Compute the frequency using the FFT centroid
nmax = power.argmax()
if (nmax > 200):
	w = 200
else:
	w = nmax

freq_centroid  = (freq[nmax-w:nmax+w] * power[nmax-w:nmax+w]).sum()
freq_centroid /= power[nmax-w:nmax+w].sum()

print "\nFrequency Weight Average (Centroid)"
print freq_centroid

# Compute the frequency using a Lorentzian fit

def lorentzian(x, x0, gamma, a, b):
	num = 0.5 * gamma * a
	den = np.pi * ((x - x0)**2 + (0.5 * gamma)**2)
	return num / den + b

# Need to estimate freq, gamma
guess = []
guess.append(freq_centroid)
freq2 = (freq[nmax-w:nmax+w] * freq[nmax-w:nmax+w] * power[nmax-w:nmax+w]).sum() 
freq2 /= power[nmax-w:nmax+w].sum()
guess.append(2 * (freq2 - freq_centroid**2)**0.5)
guess.append(power[nmax-w:nmax+w].max() * 0.5 * np.pi * guess[1])
guess.append(power[-100:].mean())

par, cov = curve_fit(lorentzian, freq[nmax-w:nmax+w], power[nmax-w:nmax+w], p0=guess)
freq_lorentzian = par[0]

print "\nResults of Lorentzian Fit: (Frequency, FWHM)"
print par[:2]

# Plot the Lorentian fit
plt.plot(freq[nmax-w:nmax+w], power[nmax-w:nmax+w], label='Spectral Power')
fit = lorentzian(freq[nmax-w:nmax+w], par[0], par[1], par[2], par[3])
plt.plot(freq[nmax-w:nmax+w], fit, label='Lorentzian Fit')
plt.title('Peak Power Fit')
plt.xlabel('Frequency')
plt.ylabel('Power [arb. units]')
plt.legend()
plt.savefig(stub + 'lor_fit.png')
plt.close()

# Try a "soft" Lorentzian with |x| instead of x**2
def soft_lorentzian(x, x0, gamma, a, b, alpha):
	num = 0.5 * gamma * a
	den = np.pi * (abs(x - x0)**alpha + (0.5 * gamma)**alpha)
	return num / den + b

# Need to estimate freq, gamma
guess = par.tolist()
guess.append(2.0)
par, cov = curve_fit(soft_lorentzian, freq[nmax-w:nmax+w], power[nmax-w:nmax+w], p0=guess)
freq_soft_lorentzian = par[0]

print "\nResults of Soft Lorentzian Fit: (Frequency, FWHM)"
print par

# Plot the Soft Lorentian fit
plt.plot(freq[nmax-w:nmax+w], power[nmax-w:nmax+w], label='Spectral Power')
fit = soft_lorentzian(freq[nmax-w:nmax+w], par[0], par[1], par[2], par[3], par[4])
plt.plot(freq[nmax-w:nmax+w], fit, label='Soft Lorentzian Fit')
plt.title('Peak Power Fit')
plt.xlabel('Frequency')
plt.ylabel('Power [arb. units]')
plt.legend()
plt.savefig(stub + 'slor_fit.png')
plt.close()


# Lorentzian fit doesn't look great, maybe try exponential
def exp_peak(x, x0, tau, c, b):
	return np.exp(-abs((x - x0)/tau) + c) + b

guess[2] = np.log(power.max())
guess.pop()

par, cov = curve_fit(exp_peak, freq[nmax-w:nmax+w], power[nmax-w:nmax+w], p0=guess)
freq_exp_peak = par[0]

print "\nResults of Exponential Fit: (Frequency, FWHM)"
print par[:2]

# Plot the Exponential fit
plt.plot(freq[nmax-w:nmax+w], power[nmax-w:nmax+w], label='Spectral Power')
fit = exp_peak(freq[nmax-w:nmax+w], par[0], par[1], par[2], par[3])
plt.plot(freq[nmax-w:nmax+w], fit, label='Exponential Fit')
plt.title('Peak Power Fit')
plt.xlabel('Frequency')
plt.ylabel('Power [arb. units]')
plt.legend()
plt.savefig(stub + 'exp_fit.png')
plt.close()


# Compute the frequency using the phase method
phase = np.unwrap(np.arctan2(wf_im, wf), discont=1.7 * np.pi)

n = 20
der = np.diff(phase) * (1 / dt)
#der = np.convolve(der, np.ones(n) * (1.0 / n) , mode='same')

# Extract frequency with linear fit
def pol1(t, w, b):
	return w * 6.2831853 * t + b

guess = []
guess.append(freq_lorentzian)
guess.append(freq_lorentzian * (-fid_i))
w = 10

par, cov = curve_fit(pol1, time[fid_i+w:fid_f-w], phase[fid_i+w:fid_f-w], p0=guess)
freq_phase = par[0]
phase_par = par

print "\nResults of fitting phase progression"
print par

i = 0
f = 10000
plt.plot(time[i:f], phase[i:f], label="Phase")
plt.plot(time[fid_i:fid_f], pol1(time[fid_i:fid_f], par[0], par[1]), label="Linear Fit")
#plt.plot(time[i:f], der[i:f], label="Phase Derivative")
plt.title("Signal Phase")
plt.xlabel("Time [ms]")
plt.ylabel("Phase")
plt.legend()
plt.savefig(stub + 'phase.png')
plt.close()

# Compute the frequency from fitting the fid / env

w = 1000
plt.plot(time[fid_i:fid_i+w], (wf / env)[fid_i:fid_i+w])
plt.title("FID Normalized by Envelope Function")
plt.xlabel("Time [ms]")
plt.ylabel("Amplitude [arb. units]")
plt.savefig(stub + 'osc.png')
plt.close()

def osc(x, w, phi, amp):
	return amp * np.cos(w * x + phi)

guess = []
guess.append(freq_lorentzian)
guess.append(1.0) # Phase, it should handle that
guess.append(1.0) # Amplitude should actually be close to 1.

par, cov = curve_fit(osc, time[fid_i:fid_f], wf[fid_i:fid_f], p0 = guess)

print "\nResults of fitting FID / Envelope"
print par

# I want to save some results as Root Histograms, so here goes.
froot = TFile('root/' + ntpath.basename(sys.argv[1].replace('.fid', '.root')), 'recreate')
#c1 = TCanvas() # Need this to save plots

# First the FID
h_fid = TH1F("h_fid", "FID; Time [ms]; Amp [arb. units]", N, time[0], time[-1])

# The FFT
h_fft = TH1F("h_fft", "FFT; Freq [kHz]; Amp [arb. units]", freq.shape[0], freq[0], freq[-1])

# Imaginary phase FID
h_fid_im = TH1F("h_fid_im", "FID - Imaginary; Time [ms]; Amp [arb. units]", N, time[0], time[-1])

# The envelope
h_env = TH1F("h_env", "Envelope; Time [ms]; Amp [arb. units]", N, time[0], time[-1])

# The phase
h_phase = TH1F("h_phase", "Phase; Time [ms]; Phase [rad]", N, time[0], time[-1])

# The phase fit residuals
h_pres = TH1F("h_pres", "Phase Fit Residuals; Time [ms]; Phase - Fit [rad]", fid_f-fid_i, time[fid_i], time[fid_f+1])

# Fill the histos
for i in range(wf.shape[0]):
	h_fid.SetBinContent(i, wf[i])
	h_env.SetBinContent(i, env[i])
	h_phase.SetBinContent(i, phase[i])

for i in range(fft.shape[0]):
	h_fft.SetBinContent(i, np.abs(fft[i]))

for i in range(fid_i, fid_f):
	h_pres.SetBinContent(i-fid_i, phase[i] - pol1(time[i], phase_par[0], phase_par[1]))

froot.Write()
froot.Close()









