#!/bin/python

import sys
import numpy as np
#import matplotlib.pyplot as plt 
import ntpath
from ROOT import *

if len(sys.argv) < 2:
	fil = 'data/sipm_only/data.txt'
else:
	fil = sys.argv[1]

if len(sys.argv) < 3:
	title = 'FID Frequencies'
else:
	title = sys.argv[2]

if len(sys.argv) < 4:
	al_arm_correction = True
else:
	al_arm_correction = bool(sys.argv[3])

# Load the data
d = np.genfromtxt(fil)
xd = d[:, 0]
df = d[:, 1]
del d

# Initialize a graph
path = ntpath.dirname(fil) + '/'
gr = TGraphErrors(fil, "%lg %lg %lg", "")

# Correct aluminum background
if (al_arm_correction == False):

	fal = TF1('fal', 'pol1')
	fal.SetParameter(0, -0.128366)
	fal.SetParameter(1, 6.07633e-5)

	x = Double(0)
	y = Double(0)
	i = 0

	while (gr.GetPoint(i, x, y) != -1):
		gr.SetPoint(i, x, y - fal.Eval(x))
		i += 1

# Fit the data
f1 = TF1("f1", "[0] + [1] * (1 / ([2] + 18 + x)^3 - 1 / ([2] + 30.7 + x)^3) + [3] * x")
f1.SetParameter(0, gr.GetMean())
f1.SetParameter(1, (df[-1] - df[0]) / (3 * (xd[-1] - xd[0]) * (30^-4 - 43^-4)))
f1.SetParameter(2, 20.0)
f1.SetParLimits(2, 15, 25)

f1.SetLineColor(kRed)

gr.Fit(f1, "QMR", "", xd[0] + 5, xd[-1] - 5)
gr.Fit(f1, "MR", "", xd[0], xd[-1])

# Print the plots
gStyle.SetOptStat(0)
gStyle.SetOptFit(1)
gr.SetTitle(title + '; Distance [mm]; f_{1} - f_{2} [kHz]')
gr.Draw()

c1.Print(path + 'fit.png')
c1.Print('fig/' + ntpath.dirname(fil).split('/')[1] + '_fit.png')
